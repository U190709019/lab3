import java.lang.Math;

public class FindPrimes {
	
	public static void main(String[] args){
		int usernum = Integer.parseInt(args[0]);
		String result = "";

		for (int i = 2; i < usernum+1; i++){
			if(isPrime(i)){
				result += "," + String.valueOf(i);
			} else if(i == 2) {
				result += "2";
			}
		}
		System.out.println(result);
	}

	public static boolean isPrime(int num){
		if (num <= 1) {
			return false;
		}
		for (int j = 2; j < Math.sqrt(num)+1; j++){
			if (num%j==0) {
				return false;
			}
		}
		return true;
	}
}
